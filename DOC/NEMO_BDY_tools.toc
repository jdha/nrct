\select@language {french}
\contentsline {chapter}{\numberline {1}Introduction}{5}{chapter.1}
\contentsline {subsubsection}{Pre v3.4 NEMO}{5}{section*.4}
\contentsline {subsubsection}{Changes between releases}{6}{section*.5}
\contentsline {subsubsection}{Future additions?}{6}{section*.6}
\contentsline {chapter}{\numberline {2}Quick Start Guide }{9}{chapter.2}
\contentsline {section}{\numberline {2.1}Overview}{9}{section.2.1}
\contentsline {section}{\numberline {2.2}Setup}{9}{section.2.2}
\contentsline {section}{\numberline {2.3}The Domain}{9}{section.2.3}
\contentsline {section}{\numberline {2.4}Namelist}{9}{section.2.4}
\contentsline {section}{\numberline {2.5}Output}{9}{section.2.5}
\contentsline {chapter}{\numberline {3}Overview of BDY Tools }{11}{chapter.3}
\contentsline {section}{\numberline {3.1}Overview}{11}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Boundary geometry}{11}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Defining the regional domain}{12}{subsection.3.1.2}
\contentsline {section}{\numberline {3.2}Grid Information}{13}{section.3.2}
\contentsline {section}{\numberline {3.3}Vertical coordinate}{13}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}S-coordinate}{13}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}unstructured open boundaries options}{13}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Post Extraction}{13}{subsection.3.3.3}
\contentsline {subsection}{\numberline {3.3.4}Input boundary data files}{14}{subsection.3.3.4}
\contentsline {subsection}{\numberline {3.3.5}Debugging}{14}{subsection.3.3.5}
\contentsline {chapter}{\numberline {4}Examples}{17}{chapter.4}
\contentsline {section}{\numberline {4.1}Simple box domain}{17}{section.4.1}
\contentsline {section}{\numberline {4.2}AMM12}{17}{section.4.2}
\contentsline {chapter}{Index}{18}{section.4.2}
